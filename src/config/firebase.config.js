import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBOolzn3vhoLa2X79iL_AXNJkmpMMndcMg",
  authDomain: "softzino-technologies.firebaseapp.com",
  projectId: "softzino-technologies",
  storageBucket: "softzino-technologies.appspot.com",
  messagingSenderId: "167523698165",
  appId: "1:167523698165:web:20a35e373cd50feef70cbc"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export {app, firebaseConfig}
import { createRouter, createWebHistory } from "vue-router";
import HomePage from "../pages/HomePage.vue";
import AbooutPage from "../pages/AboutPage.vue";
import BlogPage from "../pages/admin/BlogPage.vue";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: HomePage
    },
    {
        path: '/about',
        name: 'About',
        component: AbooutPage
    }
    ,
    {
        path: '/admin/blog',
        name: 'admin-blog',
        component: BlogPage
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});


export default router;